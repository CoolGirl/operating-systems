all:
	cd lib && make
	cd cat && make
	cd revwords && make
	cd filter && make
	cd bufcat && make
	cd foreach && make
	cd simplesh && make
	cd filesender && make
	cd bipipe && make
	cd bipiper && make
	cd chat && make
	cd ..
clean:
	cd lib && make clean
	cd cat && make clean
	cd revwords && make clean
	cd filter && make clean
	cd bufcat && make clean
	cd foreach && make clean
	cd simplesh && make clean
	cd filesender && make clean
	cd bipipe && make clean
	cd bipiper && make clean
	cd chat && make clean
	cd ..
