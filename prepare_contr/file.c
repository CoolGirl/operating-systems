//program for replacing bb on a, start at 15.42
#include<stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include<string.h>
#define BUFFER_SIZE 4096
char big_buffer [BUFFER_SIZE];
int bytes_in_buffer = 0;

void after_found_word(int n,  char* buf)
{
    memcpy(buf, big_buffer, n);
    memmove(big_buffer, big_buffer+n, n);
    bytes_in_buffer -=n;
}

int read_until(int fd, char* buf, char delimiter)
{
    int already_wrote = 0;
    int read_result = bytes_in_buffer;
    do
    {
        printf("still in while...");
        for (int i = 0; i<read_result; i++)
        {
            if (big_buffer[i] == delimiter)
            {
                after_found_word(i+1, buf+already_wrote);
                return already_wrote + i+1;
            }
        }
        already_wrote +=read_result;
        read_result = read(fd, big_buffer, BUFFER_SIZE);
        bytes_in_buffer+=read_result;
        if (read_result == -1)
        {
            return read_result;
        }
        printf("%d", read_result);
        printf("in while");
    }while(read_result);
    return already_wrote;

}

char* replace(char* buffer, char* old_value, char* new_value, int size, int old_len, int new_len)
{
    char* answer = malloc(BUFFER_SIZE);
    int i=0;
    int current_index = 0;
    while (i<size)
    {
            answer[current_index] = buffer[i];
            if (buffer[i] == old_value[0])
            {
                int flag_found = 1;
                for (int j = 1; j<old_len; j++)
                {
                    if (buffer[i+j]!=old_value[j])
                    {
                        flag_found = 0;
                        break;
                    }
                }
                if (flag_found)
                {
                    for (int j=0; j<new_len; j++)
                    {
                        answer[current_index+j] = new_value[j];
                    }
                    current_index+= new_len;
                    i+= old_len;
                    continue;
                }
            }
            i++;
            current_index++;
    }
    return answer;
}

char buffer[BUFFER_SIZE];

int main(int argc, char  ** argv)
{
    if (argc<3)
    {
        perror("Too few args");
        return -1;
    }
    while(1)
    {
        int read_result = read_until(STDIN_FILENO, buffer, '\n');
        if (read_result == -1)
        {
            perror("reading error");
            return -1;
        }
        else if (read_result == 0)
            break;
        char* replaced =  replace(buffer, argv[1], argv[2], read_result, strlen(argv[1]),
                strlen(argv[2]));
        int already_wrote = 0;
        int len = strlen(replaced);
        while (already_wrote!=len)
        {
            int write_result = write(STDOUT_FILENO, replaced+already_wrote, len-already_wrote); 
            if (write_result == -1)
                perror("writing error");
            already_wrote+=write_result;
        }
    }
    return 0;
}
