#include<errno.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<stdio.h>

#define BUFFER_SIZE 4096
char big_buf [BUFFER_SIZE];
int bytes_in_buffer = 0;

int error(char * msg)
{
    perror(msg);
    return -1;
}

void after_found_word(int n, char * buf)
{
    memcpy(buf, big_buf, n);
    memmove(big_buf, big_buf+n, n);
    bytes_in_buffer -= n;
}

int read_until(int  fd, char * buf, char delimiter)
{
    int read_result = 0;
    int current_read = bytes_in_buffer;
    while(1)
    {
        
        for (int i= 0; i<current_read; i++)
        {
            if (big_buf[i] == delimiter)
            {
                after_found_word(i+1, buf);
                buf[read_result+i+1] = '\0';
                return read_result + i +1;
            }
        }
        after_found_word(current_read, buf);
        read_result+=current_read;
        current_read = read(fd, big_buf, BUFFER_SIZE - read_result);
        if (current_read == -1)
        {
            return current_read;
        }
        else if (current_read == 0)
            break;
        bytes_in_buffer+=current_read;
    }
    return read_result;
}


char * replace (char * buffer, char * old, char * new, int old_len, int new_len)
{
    char * answer = malloc (BUFFER_SIZE);
    int i =0;
    int current_index = 0;
    int size = strlen(buffer);
    while(i<size)
    {

        answer[current_index] = buffer[i];
        if (buffer[i] == old[0])
        {
            int flag_found = 1;
            for (int j =1; j <old_len; j++)
            {
                if (buffer[i+j]!=old[j])
                {
                    flag_found = 0;
                    break;
                }
            }
            if (flag_found)
            {
                for (int j=0; j<new_len; j++)
                {
                    answer[current_index+j] = new[j];
                }
                current_index+=new_len;
                i+=old_len;
                continue;
            }
        }
        i++;
        current_index++;
    }
    return answer;
}


int main(int argc, char ** argv)
{
    
    char buffer[BUFFER_SIZE];
   /* if (argc<3)
    {
        return error("Too few args.");
    }*/
    while (1)
    {
    int read_result = read_until(STDIN_FILENO, buffer, '\n'); 
    if (read_result==-1)
    {
        return error("reading error");
    }
    else if (read_result == 0)
    {
        return 0;
    }
    char * replaced = replace(buffer, "bb", "a", 2, 1);
            int already_wrote = 0 ;
            int rep_size = strlen(replaced);
            while(already_wrote<rep_size)
            {
                int write_result = write(STDOUT_FILENO, replaced, rep_size); 
                if (write_result == -1)
                {
                    error("write error");
                }
                already_wrote+=write_result;
            }
    }
    return 0;
}
