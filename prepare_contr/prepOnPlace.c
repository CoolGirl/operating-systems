#include <stdlib.h>
#include<stdio.h>
#include<errno.h>
#include<unistd.h>
#include<string.h>

#define BUFFER_SIZE 4096
char big_buf [BUFFER_SIZE];

int error(char * msg)
{
    perror(msg);
    return -1;
}

int bytes_in_buffer = 0;
void after_found_word(int n, char * buf)
{
    memcpy(buf, big_buf, n);
    memmove(big_buf, big_buf+n, n);
    bytes_in_buffer-=n;
}

int read_until(int fd, char * buf, char delimiter)
{
    int already_read = bytes_in_buffer;
    int current_read = 0;
    do
    {
        for (int i=0; i<current_read; i++)
        {
            if (big_buf[i] == delimiter)
            {
                after_found_word(i+1, buf);
                buf[i+1+already_read]='\0';
                return already_read+i+1;
            }
        }
        already_read+=current_read;
        after_found_word(current_read, buf);
        current_read = read(fd, big_buf, BUFFER_SIZE - already_read);
        if (current_read == -1)
        {
            return -1;
        }
        bytes_in_buffer+=current_read;
    }while (current_read);
    return already_read;
}

char * replace(char* buf, char * old, char * new, int old_size, int new_size)
{
    char * replaced = malloc(BUFFER_SIZE);
    int current_index = 0 ;
    int i = 0;
    int size = strlen(buf);
    while (i<size)
    {
        replaced[current_index] = buf[i];
        if (buf[i]==old[0])
        {
            int flag_found =1;
            for (int j=1;j<old_size;j++)
            {
                if (buf[i+j]!=old[j])
                {
                    flag_found = 0;
                    break;
                }

            }
            if (flag_found)
            {
                for (int j=0; j<new_size; j++)
                {
                    replaced[current_index+j] = new[j];
                }
                i+=old_size;
                current_index+=new_size;
                continue;
            }
        }
        i++;
        current_index++;

    }
    return replaced;
}

int main (int argc, char ** argv)
{
    char buffer[BUFFER_SIZE];
    while (1)
    {
        int read_result = read_until (STDIN_FILENO, buffer, '\n'); 
        if (read_result == -1)
        {
            return error("read error");
        }
        if (read_result == 0)
        {
            break;
        }
        char * replaced = replace(buffer, argv[1], argv[2], strlen(argv[1]), strlen(argv[2]));
                int already_wrote = 0;
                while(1)
                {
                int write_result = write(STDOUT_FILENO, replaced, strlen(replaced));

                if (write_result == -1)
                {
                    return error("write error");
                }
                already_wrote+=write_result;
                if (already_wrote==strlen(replaced))
                    break;
                }
     }

                return 0;
                }
