#include<errno.h>
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

#define BUFFER_SIZE 4096

void checked_call(int result, char * msg)
{
    if (result == -1)
    {
        perror(msg);
        _exit(EXIT_FAILURE);
    }
}

void write_all(int fd, char * buffer, int count)
{
    int already_wrote  = 0;
    while (already_wrote<count)
    {
        int write_result = write(STDOUT_FILENO, buffer, count- already_wrote);
        checked_call(write_result, "error while writing");
        already_wrote+=write_result;
    }
}

int main(int argc, char ** argv)
{
    char buffer[BUFFER_SIZE];
    char prev = '\0';
    char curr = '\0';
    while(1)
    {

        buffer[0] = curr;
        int read_result = read(STDIN_FILENO, buffer+1, BUFFER_SIZE -1);
        checked_call(read_result, "error while reading");
        if (read_result == 0)
        {
            if (buffer[0]=='\0')
            {
                write_all(STDOUT_FILENO, buffer+1, 1);
            }
            else 
            {
               write_all(STDOUT_FILENO, buffer, 1); 
            }
            break;
        }
        int new_index = 0;
        for(int i=1; i<=read_result;i++) 
        {
            prev = curr;
            curr = buffer[i];
            if (prev == curr && prev == 'b')
            {
                buffer[new_index] = 'a'; 
                new_index++;
                curr = '\0';
            }
            else if (prev!='\0')
            {
                buffer[new_index] = prev;
                new_index++;
            }
        }
        write_all(STDOUT_FILENO, buffer, new_index);
    }
    return EXIT_SUCCESS;
}
