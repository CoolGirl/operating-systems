#include<errno.h>
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

#define BUFFER_SIZE 4096

int error(char * msg)
{
    perror(msg);
    return -1;
}

int central_pipe[2];

char big_buf[BUFFER_SIZE];
int bytes_in_buffer =0;

void after_found_word(int n, char * buf)
{
    memcpy(buf, big_buf, n);
    memmove(big_buf, big_buf+n, n);
    bytes_in_buffer -=n;
}

int read_until(int fd,char * buffer, char delimiter)
{
    int already_read = 0;
    int current_read = bytes_in_buffer;
    while(1)
    {
        for (int i=0; i<current_read; i++)
        {
            if (big_buf[i] == delimiter)
            {
                after_found_word(i+1, buffer+already_read);
                buffer[i+1+already_read] = '\0';
                return i+1+already_read;
            }
        }
        after_found_word(current_read, buffer+already_read);
        already_read+=current_read;
        current_read = read(fd, big_buf, BUFFER_SIZE - already_read);
        if (current_read == -1)
        {
            return -1;
        }
        else if(current_read == 0)
        {
            break;
        }
        bytes_in_buffer+=current_read;
    }
    return already_read;

}

int write_until(int fd, char *buffer, int size)
{
    int already_wrote = 0;
    int write_result = 0;
    while(1)
    {
        write_result = write(fd, buffer+already_wrote, size-already_wrote);
        if (write_result == -1)
            return -1;
        already_wrote+=write_result;
        if (already_wrote==size)
            break;
    }
}

int replacer(char* map1,  char * map2)
{
    char buffer [BUFFER_SIZE];
    while(1)
    {
        int read_result = read_until(STDIN_FILENO, buffer, '\n'); 
        if (read_result == -1)
        {
            return -1;
        }
        else if (read_result == 0)
        {
            int close_result = close(central_pipe[1]);
            if (close_result == -1)
            {
                return -1;
            }
            return 0;
        }
        for (int i=0; i<strlen(buffer); i++)
        {
            for (int j=0; j<strlen(map1); j++)
            {
                if (buffer[i] == map1[j])
                {
                    buffer[i] = map2[j];
                    break;
                }

            }
        }
        int write_result = write_until(central_pipe[1], buffer, read_result); 
        if (write_result == -1)
        {
            return -1;
        }
    }
    return 0;
}

int finder(char * pattern)
{
    char buffer[BUFFER_SIZE];
    int read_result = 0;
    while (1)
    {
        read_result = read_until(central_pipe[0], buffer, '\n'); 
        if (read_result == -1)
        {
            return -1;
        }
        else if (read_result == 0)
        {
            int close_result = close(central_pipe[0]);
            if (close_result == -1)
            {
                return -1;
            }
            return 0;
        }
        for (int i = 0;i<read_result; i++)
        {
            if (buffer[i] == pattern[0])
            {
                int flag_found = 1;
                for (int j =1; j<strlen(pattern); j++)
                {
                    if (buffer[i+j]!=pattern[j])
                    {
                        flag_found = 0;
                        break;
                    }

                }
                if (flag_found)
                {
                    write_until(STDOUT_FILENO, buffer, read_result);
                    break;
                }
            }
        }

    }
    return 0;
}

int main (int argc, char ** argv)
{
   /* if (argc<4)
    {
        return error("too few args");
    }*/
    int pipe_result = pipe(central_pipe);
    if (pipe_result == -1)
    {
        return -1;
    }
    int f = fork();
    if (f)
    {
       int finder_result = finder("cdcd");
       if (finder_result == -1)
       {
           return -1;
       }
    }
   else
    {
       int replacer_result = replacer("ab", "cd");
        return replacer_result;
    }
    return 0;
}

