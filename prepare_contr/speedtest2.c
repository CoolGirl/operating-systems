#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<unistd.h>

int error(char * msh)
{
    perror(msh);
    return -1;
}

#define BUFFER_SIZE 4096
char big_buf[BUFFER_SIZE];
int bytes_in_buffer= 0;

void after_found_word(int n ,  char * buf)
{
    memcpy(buf, big_buf, n);
    memmove(big_buf, big_buf+n, n);
    bytes_in_buffer-=n;
}

int read_until (int fd, char * buf, char delimiter)
 {
     int already_read = 0;
     int current_read = bytes_in_buffer;
     do
     {
         for (int i =0; i<current_read; i++)
        {
            if (buf[i] == delimiter)
            {
                after_found_word(i+1, buf+already_read);
                buf[already_read+i+1] = '\0';
                return already_read + i +1;
            }
        }
         after_found_word(current_read, buf+already_read);
         already_read+=current_read;
         current_read = read(fd, big_buf+already_read, BUFFER_SIZE - already_read);
         bytes_in_buffer+=current_read;
         if (current_read== -1)
         {
             return -1;
         }
         
     }while (current_read!=0);
     return already_read;
 }

char * replace(char * buf, char * old, char * new, int old_len, int new_len)
{
    char * answer = malloc(BUFFER_SIZE);
   int i = 0;
   int current_index = 0;
   int size = strlen(buf);
   while (i<size)
   {
       answer[current_index] = buf[i];
       if (buf[i]==old[0])
       {
           int flag_found = 1;
           for (int j=1; j<old_len; j++)
            {
               if (buf[i+j]!=old[j])
               {
                   flag_found = 0;
                    break;
               }
            }
           if (flag_found)
           {
               for (int j=0; j<new_len ;j++)
               {
                   answer[current_index+j] = new[j];
               }
               current_index+=new_len;
               i+=old_len;
               continue;
           }
       }
       i++;
       current_index++;
   }
   return  answer;
}

int main(int argc, char ** argv)
{
    char buf [ BUFFER_SIZE];
    if (argc<3)
    {
       return error("too few args");
    }
    int read_result = 0;
    do
    {
        read_result = read_until(STDIN_FILENO, buf, BUFFER_SIZE);
        if (read_result == -1)
        {
           return error("read error");
        }
        char * replaced = replace(buf, argv[1], argv[2], strlen(argv[1]), strlen(argv[2]));
        int already_wrote = 0;
        do{
        int write_result = write(STDOUT_FILENO, replaced, strlen(replaced)); 
        if (write_result == -1)
        {
            return error("write error");
        }
        already_wrote+=write_result;
        }while (already_wrote<strlen(replaced));
    }while (read_result!=0);
    
    return 0;
}
