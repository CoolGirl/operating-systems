#include<stdlib.h>
#include<stdio.h>
#include<errno.h>
#include<unistd.h>
#include<string.h>

#define BUFFER_SIZE 4096
char big_buf [BUFFER_SIZE];
int bytes_in_buffer = 0;

int error(char * msg)
{

    perror(msg);
    return -1;
}

void after_found_word(int n, char * buf)
{
    memcpy(buf, big_buf, n);
    memmove(big_buf, big_buf+n, n);
    bytes_in_buffer-=n;
}
int read_until(int fd, char * buf, char delimiter)
    {
        int already_read = 0;
        int current_read = bytes_in_buffer;
        do
        {
            for (int i=0; i<current_read; i++)
            {
                if (big_buf[i] == delimiter)
                {
                    after_found_word(i+1, buf+already_read);
                    return i+1+already_read;
                }
            }
            already_read+=current_read;
            after_found_word(current_read, buf+already_read);
            current_read = read(fd, big_buf, BUFFER_SIZE - already_read);
            if (current_read == -1)
                return -1;
            bytes_in_buffer+=current_read;
        }while (current_read);
        return already_read;
    }

char * replace(char * buf, char * old, char * new, int old_len, int new_len)
{
    char * ans = malloc(BUFFER_SIZE);
    int curr= 0;
    int i=0;
    int size = strlen(buf);
    while(i<size)
    {
        ans[curr] = buf[i];
        if (buf[i]==old[0])
        {
            int flag_found = 1;
            for (int j=1 ;j<old_len ;j++)
            {
                if (buf[i+j]!=old[j])
                {
                    flag_found = 0;
                    break;
                }
            }
            if (flag_found)
            {
                for (int j=0;j<new_len;j++)
                {
                    ans[curr+j] = new[j];
                }
                curr+=new_len;
                i+=old_len;

            }

        }
        i++;
        curr++;
    }
    return ans;
} 

int main(int argc, char ** argv)
{
    char buffer[BUFFER_SIZE];
    if (argc<3)
    {
        return error("few args");
    }
    while (1)
    {
        int read_result = read_until(STDIN_FILENO,buffer, '\n'); 
        if (read_result == -1)
        {
            error("read");
        }
        if (read_result == 0)
        {
            break;
        }
        char * repl = replace(buffer, argv[1], argv[2], strlen(argv[1]), strlen(argv[2]));
        int already_wrote =0;
        while (1)
        {
            int curr = write(STDOUT_FILENO, repl+already_wrote, strlen(repl) - already_wrote); 
            if (curr == -1)
            {
                error("write");
            }
            already_wrote+=curr;
            if (already_wrote == strlen(repl))
                break;
        }
    }
    return 0;
}
