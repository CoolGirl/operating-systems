#include<errno.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>

#define BUFFER_SIZE 4096
void checked_call(int result, char * msg, int status)
{
    if (result==-1)
    {
        perror(msg);
        _exit(status);
    }
}

char * replace(char * buf, char * old_val, char * new_val, int old_len, int new_len)
{
    int buffer_size = strlen(buf);
    char * answer = malloc(BUFFER_SIZE);
    int i=0;
    int curr =0;
    while (i<buffer_size)
    {
        int flag_found = 0;
        if (buffer_size-i>=old_len)
        {
            flag_found = 1;
            for (int j= 0; j<old_len; j++)
            {

                if(buf[i+j]!=old_val[j])
                {
                    flag_found = 0;
                    break;
                }

            }
        }
        if (flag_found)
        {
            memcpy(answer+curr, new_val, new_len);
            curr+=new_len;
            i+=old_len;
        }
        else
        {
            answer[curr] = buf[i];
            curr++;
            i++;
        }

    }
    answer[curr]='\0';
    return answer;

}

int main(int argc, char ** argv)
{
    char buffer[BUFFER_SIZE];
    while(1)
    {
        int read_result = read(STDIN_FILENO, buffer, BUFFER_SIZE-1);
        checked_call(read_result,
                "error while reading", EXIT_FAILURE); 
        if (read_result == 0)
        {
            break;
        }
        buffer[read_result]='\0';
        char * answer = replace(buffer, "bb", "a", 2, 1);        
        int ans_len = strlen(answer);
        int already_wrote = 0;
        while (already_wrote<ans_len)
        {
            int write_result = write(STDOUT_FILENO, answer, ans_len-already_wrote);
            checked_call(write_result,
                    "error while writing", EXIT_FAILURE);
            already_wrote += write_result; 
        }
    }
    return EXIT_SUCCESS;
}
