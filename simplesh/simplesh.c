#include <stdlib.h>
#include "helpers.h"
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <bits/sigaction.h>

#define PROMPT "$"
#define BUFFER_SIZE 4096
#define DELIMITER '\n'
#define SPLITTER '|'
#define ARGS_SPLITTER ' '
#define MAX_ARGS 2048
#define MAX_CHILDREN 100

int child_process_group = 0;
int buffer_size = 0; 

int sigint = 0;


char buffer[BUFFER_SIZE];

void int_action_handler(int signum)
{
    if (signum==SIGINT || signum ==SIGPIPE)
    {   
       
         sigint = 1;
         for (int i=0; i<children_number; i++)
         {
            kill(children_ids[i], SIGKILL);
            waitpid(children_ids[i], NULL, 0);
           

         }

       checked_call(write(STDOUT_FILENO, "\n", 1));
        buffer_size = 0; 
 
    }
    
} 
    
   
    
int main(int argc, char * argv [])
{
    //printf("%d", getpid());
    children_ids = malloc(MAX_CHILDREN*4);
    struct sigaction int_action, old_action;
    int_action.sa_handler = int_action_handler;
    int_action.sa_flags = 0;
    sigemptyset(&int_action.sa_mask);
    sigaction(SIGINT, &int_action ,&old_action);
    sigaction(SIGPIPE, &int_action, &old_action);
    while (1)
    {
        
       // printf("%d", getpid());
        
        checked_call(write(STDOUT_FILENO, PROMPT, strlen(PROMPT)));
       
    	sigint = 0;
    	//printf("%d", getpid());
    	while(1)
    	{
		    size_t read_result = checked_call(read_until(STDIN_FILENO,buffer, BUFFER_SIZE, DELIMITER ));
		    if (sigint == 1)
		        break;
		    if (read_result!=-1)
		        buffer_size = read_result;
	        if (read_result == 0)
	        {
	                for (int i=0; i<children_number; i++)
                 {
                    kill(children_ids[i], SIGKILL);
                 }
		          	exit(0);
	        }
	        if (buffer_size!=0)
	        {
		        if (buffer[read_result - 1] == '\n') {
            		buffer[read_result - 1] = '\0';
            		--read_result;
            		}
	            char** commands = split(buffer, read_result, SPLITTER);
	            size_t current_command_pos = 0;
	            char* current_command = commands[current_command_pos];
	           // char* current_command = trim(current_command_without_trim);	    
	            execargs_t** execargs = calloc(MAX_ARGS, sizeof(execargs_t));
	            while (current_command!=0)
	            {
	                current_command = trim(current_command);
		            char** command_with_args = split(current_command,strlen(current_command) , ARGS_SPLITTER);
		            char* command_name = command_with_args[0];
		            int args_counter =1;
		            char * current_arg = command_with_args[args_counter];
		            while (current_arg!=0)
		            {
			            current_arg = command_with_args[++args_counter];		
		            }
		            args_counter--;
		            execargs[current_command_pos] = createExecargs_t(command_name, command_with_args+1, args_counter);
		            current_command = commands[++current_command_pos];
	            }

            	    runpiped(execargs, current_command_pos);  
        	    children_number = 0;
        	}
        	break;
        }
/*		char** ls = malloc(100);
		ls[0]="ls\0";
	execargs_t** execargs = malloc(100);
	char*p =0;
	execargs[0] = createExecargs_t("ls\0",&p, 0);
	runpiped(execargs, 1);
 */   

    }
}
