#include "../lib/helpers.h"
#include <errno.h>

#define BUFFER_SIZE 4097

char buffer[BUFFER_SIZE];


void reverse(char* buf, size_t size)
{
		for (size_t i=0;i<size/2;i++)
		{
			char temp = buf[i];
			buf[i] = buf[size - 1 - i];
			buf[size - 1 - i] = temp;	
		}
}

int main() 
{
	ssize_t last_read = 0;
	do
 	{
		ssize_t bytes_written = 0;
		while (bytes_written < last_read) 
		{
			
		    ssize_t write_result = write_(STDOUT_FILENO, buffer + bytes_written, last_read - bytes_written);
			if (write_result == -1) 
			{
				return errno;
			}
			bytes_written += write_result;
		}
 		last_read = read_until(STDIN_FILENO, buffer, BUFFER_SIZE,' ');
		if (last_read == -1) 
		{
			return errno;
		}
		reverse(buffer, buffer[last_read - 1] == ' ' ? last_read - 1 : last_read);	
	} while (last_read > 0);	
	return 0;
}

