#include "../lib/helpers.h"
#include <string.h>
#include <errno.h>

#define BUFFER_SIZE 4096
#define MAX_ARGS 256

char buffer[BUFFER_SIZE];

int main(int argc, char* argv[])
{
	char* args[MAX_ARGS];
	memcpy(args,argv+1,(argc-1)*sizeof(char*));
	args[argc-1] = buffer;
	args[argc] = 0;

	ssize_t last_read = read_until(STDIN_FILENO, buffer, BUFFER_SIZE-2, '\n');
	while (last_read!=0)
	{
		if (last_read == -1)
			return errno;
		buffer[buffer[last_read-1] == '\n' ? last_read-1 : last_read] = 0;
		if (!spawn(argv[1], args))
		{
			int write_number;
			if (buffer[last_read]-1==0)
			{
				write_number = last_read+1;
				buffer[last_read-1] = '\n';
				buffer[last_read]=0;
			}
			else
			{
				write_number = last_read+2;
				buffer[last_read] = '\n';
				buffer[last_read+1]=0;
			}
			ssize_t write_result = write_(STDOUT_FILENO, buffer, write_number);
			if (write_result == -1)
				return errno;
		}	
		last_read = read_until(STDIN_FILENO, buffer, BUFFER_SIZE-1,'\n');
	}
	return 0;
}