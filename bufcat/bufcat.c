#include "bufio.h"
#define BUFFER_CAPACITY 4096

int main()
{
		buf_t* buf = buf_new(BUFFER_CAPACITY);
		ssize_t read_res = 0;
		do
		{
			 read_res = buf_fill(STDIN_FILENO, buf, buf_capacity(buf));
			 ssize_t write_res = buf_flush(STDOUT_FILENO, buf, buf_size(buf));
				if (write_res == -1)
						return -1;
		}while (read_res>0);
		buf_free(buf);
		return 0;
}
		
