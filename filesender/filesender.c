#include "bufio.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <stdio.h>

#define BUFFER_SIZE 4096 
int main(int argc, char * argv [])
{

	if (argc<3)
		return -1;
	buf_t* buffer = buf_new(BUFFER_SIZE);
		int port = atoi(argv[1]);
	  int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	    if (sock == -1)
		        perror("socket");
	     // printf("sock = %d\n", sock);
	        int one = 1;
		  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(int)) == -1)
			      perror("setsockopt");
		    struct sockaddr_in addr = {
			        .sin_family = AF_INET,
				    .sin_port = htons(port),
				        .sin_addr = {.s_addr = INADDR_ANY}};
		      if (bind(sock, (struct sockaddr*)&addr, sizeof(addr)) == -1)
			          perror("bind");
		        if (listen(sock, 1) == -1)
				    perror("listen");
			  struct sockaddr_in client;
			    socklen_t sz = sizeof(client);
			    while (1)
			    {
				    
			      int fd = accept(sock, (struct sockaddr*)&client, &sz);
			        if (fd == -1)
					    perror("accept");
				int f = fork();
				if (!f)
				{
					int open_result = open(argv[2],O_RDONLY);	
					if (open_result == -1)
						perror("open");
					int current_read =  0;
					do
					{
						current_read =buf_fill(open_result, buffer, BUFFER_SIZE);
						if (current_read == -1)
							perror("read");
						if (buf_flush(fd, buffer,current_read ) == -1)
						    perror("write");

					}while(current_read!=0);
					int close_result = close(fd);
				if (close_result==-1)
					perror("close");

				 // printf("accept = %d\n", fd);
				 //   printf("from %s:%d\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
				     // char hello[] = "hello\n";
				        					  			  
			    	}
				else
				{
					int close_result = close(fd);
					if (close_result==-1)
						perror("close");
				}
			    }

						    return 0;
}

