#include<stdlib.h>
#include<stdio.h>
#include<errno.h>
#include<unistd.h>
#include<string.h>
#include<signal.h>

void checked_call(int result, char * msg)
{
    if (result == -1)
    {
        perror(msg);
        _exit(EXIT_FAILURE);
    }

}

void new_action_handler(int signum)
{
    if (signum == SIGCHLD)
    {
       checked_call(sa_status, "error in replacer"); 
    }
}

void write_all(int fd, char * buf, int count)
{
    int already_wrote = 0;
    while(already_wrote<count)
    {
        int write_result = write(fd, buf+already_wrote, count-already_wrote);
        checked_call(write_result, "error while writing");
        already_wrote+=write_result;
    }
}

int main(int argc, char ** argv)
{
    if (argc < 4)
    {
       checked_call(-1, "too few args"); 
    }
    struct sigaction new_action, old_action;
    new_action.sa_handler = new_action_handler;
    new_action.sa_flag = 0;
    sigemptyset(&new_action.sa_mask);
    sigaction(SIGCHLD, &new_action, &old_action);
    int f = fork();
    if (f)
    {
        checked_call(result, "error in finder");
       /* int status;
        wait(&status);
        if (WIFEXITED(status))
        {
            checked_call(WEXITSTATUS(status), "error in replacer, terminated normally");
        }
        else
        {
            checked_call(-1, "error in replacer, terminated unnormally");
        }*/

    }
    else
    {
        return -1;
    }
    return EXIT_SUCCESS;
}
