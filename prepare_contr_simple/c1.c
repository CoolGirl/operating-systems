#include<errno.h>
#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>

#define BUFFER_SIZE 4096
void checked_call(int result, char * msg)
{
    if (result == -1)
    {
        perror(msg);
        _exit(EXIT_FAILURE);
    }
}

void write_all(int fd, char * buf, int count)
{
    int already_wrote = 0;
    while(already_wrote<count)
    {
        int write_result = write(fd, buf+already_wrote, count-already_wrote);
        checked_call(write_result, "error while writing");
        already_wrote+=write_result;
    }
}
int main(int argc, char ** argv)
{
    char buffer[BUFFER_SIZE];
    char buffer2[BUFFER_SIZE];
    char b = 'b';
    char a = 'a';
    int f = 0;
    while (1)
    {
        int read_result = read(STDIN_FILENO, buffer, BUFFER_SIZE);
        checked_call(read_result, "error while reading");
        if (!read_result)
        {
            if (f)
            {
                write_all(STDOUT_FILENO, &b, 1);
            }
            break;
        }
        int new_index = 0;
        for (int i=0; i<read_result;i++)
        {
            if (buffer[i]!=b)
            {
                if (f)
                {
                    buffer2[new_index++] = b;
                    f = 0;
                }
               buffer2[new_index++] = buffer[i]; 
            }
            else
            {
                if (f)
                {
                    buffer2[new_index++] = a;
                    f = 0;
                }
                else
                {
                    f = 1;
                }
            }
        }
        write_all(STDOUT_FILENO, buffer2, new_index);
    }
    return EXIT_SUCCESS;
}
