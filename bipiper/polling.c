#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <poll.h>
#include <errno.h>
#include "bufio.h"

#define BUFFER_SIZE 4096
#define FDS 256

int first_fds = 0;
int second_fds = 0;
int last_fd = 1; // must be even
buf_t bufs[FDS];
struct pollfd fds[FDS];




void fillPollFd(int index, int fd, short events)
{

    fds[index].fd = fd;
    fds[index].events = events;
}

void newFdAppeared(struct pollfd fd_struct, struct sockaddr_in client, socklen_t sz, int first_in_pair)
{
    if (fd_struct.revents == POLLIN)
    {
        int fd1 = accept(fd_struct.fd, (struct sockaddr*)&client, &sz); 
        if (fd1 != -1 && last_fd != FDS)
        {
            if (first_in_pair)
            {
                fillPollFd(2+ 2*first_fds, fd1, 0);
                first_fds++;
            } 
            else
            {
                fillPollFd(3+2*second_fds, fd1, 0);
                second_fds++;
            }
            last_fd+=2;
        }      
    }

}


void swap_pair(int index)
{
    if (index & 1)
        index --;
    close(fds[index].fd);
    close(fds[index+1].fd);

    fds[index] = fds[last_fd-1];
    fds[index+1] = fds[last_fd];
    bufs[index] = bufs[last_fd-1];
    bufs[index+1] = bufs[last_fd];
    bufs[index].size = 0;
    bufs[index+1].size = 0;
    last_fd-=2;
    first_fds--;
    second_fds--;
}

int has_pair(int index)
{
    if (index & 1)
    {
        if (fds[index-1].fd!=0)
        return index-1;
    }
    else if (fds[index+1].fd!=0)
        return index+1;
    return 0;
}

#define BUFFER_SIZE 4096
int main(int argc, char * argv [])
{

    if (argc<3)
        perror("too few arguments");
    for (int i=0; i<FDS; i++)
    {
        bufs[i] = * buf_new(BUFFER_SIZE);
    }


    int port = atoi(argv[1]);
    int port2 = atoi(argv[2]);
    //int port = 1234;
   // int port2 = 4321;
    int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    int sock2 = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (1 == -sock || sock2 == -1)
        perror("socket");
    // printf("sock = %d\n", sock);
    int one = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(int)) == -1)
        perror("setsockopt");
    if (setsockopt(sock2, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(int)) == -1)
        perror("setsockopt");
    struct sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port = htons(port),
        .sin_addr = {.s_addr = INADDR_ANY}};
    if (bind(sock, (struct sockaddr*)&addr, sizeof(addr)) == -1)
        perror("bind");
    if (listen(sock, 1) == -1)
        perror("listen");
    struct sockaddr_in client;
    socklen_t sz = sizeof(client);
    struct sockaddr_in addr2 = {
        .sin_family = AF_INET,
        .sin_port = htons(port2),
        .sin_addr = {.s_addr = INADDR_ANY}};
    if (bind(sock2, (struct sockaddr*)&addr2, sizeof(addr2)) == -1)
        perror("bind");
    if (listen(sock2, 1) == -1)
        perror("listen");
    struct sockaddr_in client2;
    socklen_t sz2 = sizeof(client2);

    fillPollFd(0, sock, POLLIN);
    fillPollFd(1, sock2, POLLIN);



    while (1)
    {

        int poll_result = poll(fds, last_fd+1,-1);
        if (poll_result == -1)
            if (poll_result != EINTR)
                perror("poll");
        newFdAppeared(fds[0], client, sz, 1);
        newFdAppeared(fds[1], client2, sz2, 0);
        for (int i=2; i<=last_fd; i++)
        {
            if (fds[i].revents & POLLIN )
            {
                int current_read =buf_fill(fds[i].fd, & bufs[i], 1);
                if (current_read <=0)
                {
                    int pair = has_pair(i);
                    if (pair)
                    {    
                        int shutdown_result = shutdown(fds[pair].fd, SHUT_RD);
                        if (shutdown_result == -1)
                            swap_pair(i);
                    }
                    else
                        close(fds[i].fd);
                    fds[i].fd = -1;
                     
                }
            }


        }

        for (int i=2; i<=last_fd; i++)
        {
            if (fds[i].revents & POLLOUT)
            {
                int pair = has_pair(i);
                int write_result = buf_flush(fds[i].fd, & bufs[pair], 1);
                if (bufs[pair].size == 0 && fds[pair].fd ==-1)
                {
                    swap_pair(i);
                }
                if (write_result == -1)
                    swap_pair(i);    
            }         
        }

        for (int i=2; i<=last_fd; i++)
        {
            if (fds[i].fd!=0)
            {
                if (bufs[i].size!= bufs[i].capacity)
                {
                    fds[i].events = POLLIN;
                }

                int pair = has_pair(i);
                if (pair)
                {
                    if (bufs[pair].size!=0)
                        fds[i].events|=POLLOUT;
                }
            }
        }


    }
    return 0;
}


