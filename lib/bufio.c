#include "bufio.h"
#include <stdlib.h>
#include <string.h>

static inline void debug_abort()
{
	#ifdef DEBUG
		abort();
	#endif
}

 buf_t* buf_new(size_t capacity) 
{
	 buf_t* result =	( buf_t*) (malloc(sizeof(buf_t)));
	if(!result)
			return result;
	result->capacity = capacity;
	result->data = (char*) (malloc(capacity));
    return result;	
}

void buf_free( buf_t * buf)
{
	if (buf==0)
		debug_abort();
	free(buf->data);		
	free(buf);
}

 size_t buf_capacity(buf_t * buf)
 {
	if (buf == 0) 
		debug_abort();
	return buf->capacity; 
 }

 size_t buf_size(buf_t * buf)
 {
		 
	if (buf==0)
		debug_abort(); 
		 return buf->size;
 }

 ssize_t buf_fill(fd_t fd, buf_t *buf, size_t required)
 {
		 
		
	 if (buf==0)
    	debug_abort(); 
	 while(buf->size<required)
	 {
			ssize_t last_read = read(fd,buf->data+buf->size,buf->capacity-buf->size);
			if (last_read==-1)
					return -1;
			if (last_read == 0)
					break;
			buf->size+=last_read;
			if (buf->size>buf->capacity)
				debug_abort();
	 }	
	 return buf->size;
 }

 ssize_t buf_flush(fd_t fd, buf_t *buf, size_t required)
 {
	if (buf==0)
		debug_abort(); 
	size_t already_written = 0;
	ssize_t last_write = 0;
	while(already_written<required)
	{
			 last_write = write(fd, buf->data+already_written, buf->size - already_written);
			if (last_write == -1)
					break;
			already_written+=last_write;
			
	}
	memmove(buf->data, buf->data+already_written, buf->size-already_written);
	buf->size-=already_written;
	if (last_write==-1)
		return -1;
	return already_written;	
 }

ssize_t buf_write(fd_t fd, buf_t *buf, size_t required)
 {
	if (buf==0)
		debug_abort(); 
	size_t already_written = 0;
	ssize_t last_write = 0;
	while(already_written<required)
	{
			 last_write = write(fd, buf->data+already_written, buf->size - already_written);
			if (last_write == -1)
					break;
			already_written+=last_write;
			
	}
	if (last_write==-1)
		return -1;
	return already_written;	
 }
#define BUFFER_SIZE 4097
 
size_t bytes_in_buffer = 0;
char delimiter = '\n';

void after_found_line(void *data, char* dest, size_t n)//it must take current_read%BUFFER_SIZE
{
	memcpy(dest, data, n);
	memmove(data, data+n, bytes_in_buffer - n);
	bytes_in_buffer -= n;	
}	


 ssize_t buf_getline(fd_t fd, buf_t *buf, char* dest)
 {
	size_t count = BUFFER_SIZE;
	size_t current_read = 0;
	while (current_read < count) 										  
	{
	    for (size_t j = 0; j < bytes_in_buffer && current_read + j < count; j++)
		{
			if (*((char*)buf->data + j) == delimiter)
			{
				after_found_line(buf->data+current_read, dest, j+1);
				return current_read + j + 1;
			}
		}	
		size_t bytes_to_copy = current_read + bytes_in_buffer;
		memcpy(dest+current_read, buf->data, bytes_to_copy);
		current_read += bytes_to_copy;
		bytes_in_buffer = 0;
		ssize_t result = buf_fill(fd, buf, BUFFER_SIZE);
	   	if (result <= 0)
		{	
			if(current_read==0)
		    {
				return result;
		 	}
			else
			{
				return current_read;
			}
		}
		bytes_in_buffer = result;
	}
	return count;
 }
 






