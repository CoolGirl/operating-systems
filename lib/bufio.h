#include <unistd.h>
#define fd_t int

typedef struct
{
		size_t capacity;
		size_t size;
		char* data;
}buf_t ;

extern  buf_t *buf_new(size_t capacity);
extern void buf_free( buf_t *);
extern size_t buf_capacity(buf_t *);
extern size_t buf_size(buf_t *);
extern ssize_t buf_fill(fd_t fd, buf_t *buf, size_t required);
extern ssize_t buf_flush(fd_t fd, buf_t *buf, size_t required);
extern ssize_t buf_getline(fd_t fd, buf_t *buf, char* dest);
