#ifndef HELPERS_H
#define HELPERS_H
#define MAX_ARGS 80

#include <unistd.h>
#include<errno.h>


int* children_ids;
size_t children_number = 0;

typedef struct {
    char *name;
    char *args[MAX_ARGS];
    size_t args_number;
} execargs_t;

extern char* trim(char* string);
extern ssize_t read_(int fd, void *buf, size_t count);
extern ssize_t write_(int fd, const void *buf, size_t count);
extern ssize_t read_until(int fd, void * buf, size_t count, char delimiter);
extern int spawn(const char * file, char * const argv []);
extern execargs_t* createExecargs_t(const char* file, char ** args, size_t args_number);
extern int runpiped(execargs_t **programs, size_t n);
extern int checked_call(ssize_t result);
extern char** split( char* buffer, size_t size, char delimiter);

#endif
