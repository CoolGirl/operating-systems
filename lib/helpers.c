#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "helpers.h"

#define BUFFER_SIZE 4096
#define MAX_PIPES 2048

char* trim(char* string)
{
	/*while (*string == ' ')
	{
		string++;
	}	
	/*char* right_end = string + strlen(string) - 1;
	while(*right_end == ' ' && right_end > string)
	{
	    *right_end = '\0';
	    right_end--;
	}
	return string;*/
	int length = strlen(string);
	int skip_from_beginning = 0;
	while (string[skip_from_beginning] == ' ')
	{
		skip_from_beginning++;
	}
	while(string[length-1] == ' ')
	{
		length--;
	}
	char* new_string = malloc(length-skip_from_beginning);
	memcpy(new_string, string+skip_from_beginning, length - skip_from_beginning);
	return new_string;
}
char** split(char* buffer, size_t size, char delimiter)
{
    char** result = calloc(MAX_PIPES, sizeof(char*));
    result[0] = buffer;
    int result_size = 1;
    for (int i=0; i<size; i++)
    {
        if (buffer[i] == delimiter)
        {
            buffer[i] = '\0';
            result[result_size++] = buffer+i+1;
        }
    }
    return result;
}

int checked_call(ssize_t result) {
    if (result == -1)
        return errno;
    return result;
}

execargs_t* createExecargs_t(const char *file, char **args, size_t args_number) {
    execargs_t *execArgs = (execargs_t *) calloc(sizeof(execargs_t), 1);
    execArgs->name = file;
	execArgs->args[0] = execArgs -> name;
    memcpy(execArgs->args + 1, args, args_number * sizeof(char*));
    execArgs->args_number = args_number;
    return execArgs;
}

typedef struct {
    int read_end;
    int write_end;
} my_pipe;


int exec(execargs_t *args) {
    return execvp(args->name, args->args);
}

my_pipe left_pipe;
my_pipe right_pipe;

void prepare_process_for_exec(int first, int last, execargs_t * program) 
{
    int f = checked_call(fork());
    if (!f)
    {        
        if (!first) 
        {
            int result = checked_call(dup2(left_pipe.read_end, STDIN_FILENO));
            close(left_pipe.read_end);
        }
        if (!last) 
        {
            checked_call(close(right_pipe.read_end));
            int result =  checked_call(dup2(right_pipe.write_end, STDOUT_FILENO));
            close(right_pipe.write_end);
        }
        int exec_result = checked_call(exec(program));
    }
    else
    {
         children_ids[children_number] = f;
        children_number++;  
    }
 }

int runpiped(execargs_t **programs, size_t n) 
{
    //checked_call(setpgid(0, 0));
   // children_ids[children_number] = getpid();
    //        children_number++;
            
    left_pipe.read_end = -1;
    left_pipe.write_end = -1;
    right_pipe.read_end = -1;
    right_pipe.write_end = -1;
    
    for (int i = 0; i < n; i++) 
    { 
        if (left_pipe.read_end != -1) 
        {
            close(left_pipe.read_end);
        }
        if (right_pipe.write_end != -1)
        {
            close(right_pipe.write_end);
        }
        left_pipe = right_pipe;
        if (i != n - 1) {
            checked_call(pipe((int*)&right_pipe));          
        }
      
        prepare_process_for_exec(i == 0, i == n - 1, programs[i]);         
    }
    
    close(left_pipe.read_end);
        
    for (int i = 0; i < n; i++)
    {
        int status;
        wait(&status);
    }
    return 0;
}

ssize_t read_(int fd, void *buf, size_t count) {
    ssize_t current_read;
    size_t already_read = 0;
    while (already_read < count) {
        current_read = read(fd, buf + already_read, count - already_read);
        if (current_read != -1) {
            already_read += current_read;
            if (current_read == 0) {
                return already_read;
            }
        }
        else {
            return already_read == 0 ? -1 : (ssize_t) already_read;
        }
    }
    return count;
}

ssize_t write_(int fd, const void *buf, size_t count) {
    ssize_t current_write;
    size_t already_write = 0;
    while (already_write < count) {
        current_write = write(fd, buf + already_write, count - already_write);
        if (current_write != -1) {
            already_write += current_write;
        }
        else {
            return already_write == 0 ? -1 : (ssize_t) already_write;
        }
    }
    return count;
}

char big_buf[BUFFER_SIZE]; //it's really bigger than the others
size_t bytes_in_buffer = 0;

void after_found_word(void *buf, size_t n)//it must take current_read%BUFFER_SIZE
{
    memcpy(buf, big_buf, n);
    memmove(big_buf, big_buf + n, bytes_in_buffer - n);
    bytes_in_buffer -= n;
}

ssize_t read_until(int fd, void *buf, size_t count, char delimiter) {
    size_t current_read = 0;
    while (current_read <
           count)  // contract:big_buf must be dumped into buf and big_buf must point to original beginning
        // of big_buf
    {
        for (size_t j = 0; j < bytes_in_buffer && current_read + j < count; j++) {
            if (*((char *) big_buf + j) == delimiter) {
                after_found_word(buf + current_read, j + 1);
                return current_read + j + 1;
            }
        }
        size_t bytes_to_copy = current_read + bytes_in_buffer <= count ? bytes_in_buffer : count - current_read;
        memcpy(buf + current_read, big_buf, bytes_to_copy);
        current_read += bytes_to_copy;
        bytes_in_buffer = 0;
        ssize_t result = read(fd, big_buf, BUFFER_SIZE);
        if (result <= 0) {
            if (current_read == 0) {
                return result;
            }
            else {
                return current_read;
            }
        }
        bytes_in_buffer = result;
    }
    return count;
}

int spawn(const char *file, char *const argv[]) {
    int f = fork();
    if (f) {
        int status;
        wait(&status);
        if (WIFEXITED(status))
            return WEXITSTATUS(status);
        return -1;
    }
    else {
        return execvp(file, argv);
    }
}		
