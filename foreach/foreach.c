#include "../lib/bufio.h"
#include "../lib/helpers.h"
#include <string.h>
#include <errno.h>

#define BUFFER_CAPACITY 4097
#define MAX_ARGS 256

char* line[BUFFER_CAPACITY];

int main(int argc, char* argv[])
{
	buf_t* buffer = buf_new(BUFFER_CAPACITY);
	char* args[MAX_ARGS];
	memcpy(args,argv+1,(argc-1)*sizeof(char*));
	args[argc-1] = buffer->data;
	args[argc] = 0;
	ssize_t last_read = buf_getline(STDIN_FILENO, buffer, line);
	write_(STDOUT_FILENO, line, last_read);
	while (last_read!=0)
	{ 
		if (last_read == -1)
			return errno;
		if (last_read%2==0)
		{
			line[last_read-1] = 0;
			if (!spawn(argv[1], args))
			{
				int write_number;
					write_number = last_read+1;
					line[last_read-1] = '\n';
					line[last_read]=0;
				ssize_t write_result = write_(STDOUT_FILENO, line, write_number);
				if (write_result == -1) 
					return errno;
			}	
		}
		last_read = buf_getline(STDIN_FILENO, buffer, line);
	}
	buf_free(buffer);
	return 0;
}
