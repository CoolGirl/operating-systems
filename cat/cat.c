#include "../lib/helpers.h"
#include <errno.h>

#define BUFFER_SIZE 1024

char buffer[BUFFER_SIZE];

int main() 
{
	int last_read = 0;
	do
 	{
		int bytes_written = 0;
		while (bytes_written < last_read) 
		{
		    int write_result = write_(STDOUT_FILENO, buffer + bytes_written, last_read - bytes_written);
			if (write_result == -1) 
			{
				return errno;
			}
			bytes_written += write_result;
		}
 		last_read = read_(STDIN_FILENO, buffer, BUFFER_SIZE);
		if (last_read == -1) 
		{
			return errno;
		}
	} while (last_read > 0);	
	return 0;
}
