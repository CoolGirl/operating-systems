#include "bufio.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <stdio.h>

#define BUFFER_SIZE 4096

void performIO(int fd, int fd2, buf_t* buffer)
{
    int current_read =  0;
    do
    {
	    current_read =buf_fill(fd, buffer, 1);
	    if (current_read == -1)
		    perror("read");
	    if (buf_flush(fd2, buffer,current_read ) == -1)
		    perror("write");
    }while(current_read!=0);
					
}
 
void closeFDS(int fd, int fd2)
{
	int close_result = close(fd);
	int close_result2 = close(fd2);
	if (close_result == -1 || close_result2 == -1)
		perror("close");			        					  			  
					
}
int main(int argc, char * argv [])
{

	if (argc<3)
		return -1;
	buf_t* buffer = buf_new(BUFFER_SIZE);
        buf_t* buffer2 = buf_new(BUFFER_SIZE);
	
		int port = atoi(argv[1]);
		int port2 = atoi(argv[2]);
	  int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	  int sock2 = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
 
	    if (sock == -1 || sock2 == -1)
		        perror("socket");
	     // printf("sock = %d\n", sock);
	        int one = 1;
		  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(int)) == -1)
			      perror("setsockopt");
		  if (setsockopt(sock2, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(int)) == -1)
			      perror("setsockopt");
		    struct sockaddr_in addr = {
			        .sin_family = AF_INET,
				    .sin_port = htons(port),
				        .sin_addr = {.s_addr = INADDR_ANY}};
		      if (bind(sock, (struct sockaddr*)&addr, sizeof(addr)) == -1)
			          perror("bind");
		        if (listen(sock, 1) == -1)
				    perror("listen");
			  struct sockaddr_in client;
			    socklen_t sz = sizeof(client);
				struct sockaddr_in addr2 = {
			        .sin_family = AF_INET,
				    .sin_port = htons(port2),
				        .sin_addr = {.s_addr = INADDR_ANY}};
		      if (bind(sock2, (struct sockaddr*)&addr2, sizeof(addr2)) == -1)
			          perror("bind");
		        if (listen(sock2, 1) == -1)
				    perror("listen");
			  struct sockaddr_in client2;
			    socklen_t sz2 = sizeof(client2);
			
			    while (1)
			    {
				    
			      int fd = accept(sock, (struct sockaddr*)&client, &sz);
			        if (fd == -1)
					    perror("accept");
				int fd2 = accept(sock2, (struct sockaddr*)&client2, &sz2);
			        if (fd2 == -1)
					    perror("accept");
				int f = fork();
				if (!f)
				{
					 performIO( fd,  fd2, buffer);
					 closeFDS(fd, fd2);	
				}
				else
				{
					int f2 = fork();
					if (!f2)
					{
						performIO(fd2, fd,buffer2);		
						closeFDS(fd, fd2);
					}
					else
					{
						closeFDS(fd, fd2);	
					}
				}
				
				
			  }

			  return 0;
}


