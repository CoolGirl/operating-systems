#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <poll.h>
#include <errno.h>
#include "bufio.h"

#define BUFFER_SIZE 4096
#define FDS 256

int last_fd = 0; // must be even
struct pollfd fds[FDS];




void fillPollFd(int index, int fd, short events)
{

    fds[index].fd = fd;
    fds[index].events = events;
}

void newFdAppeared(struct pollfd fd_struct, struct sockaddr_in client, socklen_t sz, int first_in_pair)
{
    if (fd_struct.revents == POLLIN)
    {
        int fd1 = accept(fd_struct.fd, (struct sockaddr*)&client, &sz); 
        if (fd1 != -1 && last_fd != FDS)
        {
           last_fd++;
           fillPollFd(last_fd, fd1, 0);
        }      
    }

}


void swap(int index)
{
    close(fds[index].fd);
    if (index!=1) 
    {
        fds[index] = fds[last_fd-1];
    }
    last_fd--;
}



#define BUFFER_SIZE 4096
int main(int argc, char * argv [])
{
    buf_t* buf = buf_new(BUFFER_SIZE);
    //if (argc<2)
      //  perror("too few arguments");
    


    //int port = atoi(argv[1]);
    int port = 1234;
    int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (1 == -sock)
        perror("socket");
    int one = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(int)) == -1)
        perror("setsockopt");
    struct sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port = htons(port),
        .sin_addr = {.s_addr = INADDR_ANY}};
    if (bind(sock, (struct sockaddr*)&addr, sizeof(addr)) == -1)
        perror("bind");
    if (listen(sock, 1) == -1)
        perror("listen");
    struct sockaddr_in client;
    socklen_t sz = sizeof(client);
        fillPollFd(0, sock, POLLIN);



    while (1)
    {

        int poll_result = poll(fds, last_fd+1,-1);
        if (poll_result == -1)
            if (poll_result != EINTR)
                perror("poll");
        newFdAppeared(fds[0], client, sz, 1);
        buf->size = 0;

        for (int i=1; i<=last_fd; i++)
        {
            if (fds[i].fd!=-1)
            {
                if (buf->size!=buf->capacity)
                {
                    fds[i].events = POLLIN;
                }
            }
        }

        for (int i=1; i<=last_fd; i++)
        {
            if (fds[i].revents & POLLIN )
            {
                int current_read =buf_fill(fds[i].fd, buf, 1);
                if (current_read <=0)
                {
                   swap(i);
                }
               
            }


        }

        if (buf->size!=0)
        for (int i=1; i<=last_fd; i++)
        {
            if (fds[i].fd!=-1)
            {
                    fds[i].events |= POLLOUT;
            }
        }

        for (int i=1; i<=last_fd; i++)
        {
            if (fds[i].fd!= -1 && fds[i].revents & POLLOUT)
            {
                int result = buf_write(fds[i], buf, buf->size);  
                if (result == -1)
                {
                   swap(i);
                }
            }         
        }
    }
    return 0;
}


