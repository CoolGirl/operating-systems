#include<errno.h>
#include<string.h>
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>

#define BUFFER_SIZE 4096

int central_pipe[2];

int checked_call(int result, char * msg)
{
    if (result == -1)
    {
        perror(msg);
        _exit(EXIT_FAILURE);
    }
    return result;
}

void write_all(int fd,char* buf, int count)
{
    int already_wrote = 0;
    while(already_wrote<count)
    {
        already_wrote+=checked_call(write(fd, buf+already_wrote, count-already_wrote), "error in write_all");
    }
}

char big_buf[BUFFER_SIZE];
int bytes_in_buffer = 0;

void after_found_word(char * buf, int n)
{
    memcpy(buf, big_buf, n);
    memmove(big_buf, big_buf+n, bytes_in_buffer - n);
    bytes_in_buffer -= n;
}

int read_until(int fd, char * buf, int count, char delimiter)
{
    int current_read = 0;
    while(current_read<count)
    {
        for (int i =0; i<bytes_in_buffer && i+current_read<count; i++)
        {
            if (big_buf[i]==delimiter)
            {
                after_found_word(buf+current_read, i+1);
                return current_read +i+1;
            }
        }
        int bytes_to_copy = bytes_in_buffer+current_read<count? bytes_in_buffer: count- current_read;
        memcpy(buf+current_read, big_buf, bytes_to_copy);
        current_read+=bytes_to_copy;
        bytes_in_buffer = 0;
        int read_result =  read(fd, big_buf, BUFFER_SIZE);
        if (read_result<=0)
        {
            if (current_read == 0)
            {
                return read_result;
            }
            else
            {
                return current_read;
            }
        }
        bytes_in_buffer = read_result;
    }
    return count;
}

void replace(char * old_val, char * new_val)
{
    int old_len = strlen(old_val);
    int new_len = strlen(new_val);
    char buffer[BUFFER_SIZE];
    char replaced[BUFFER_SIZE];
    while(1)
    {
        int read_result = checked_call(read_until(STDIN_FILENO, buffer, BUFFER_SIZE, '\n'), "read in replace");
        if (read_result == 0)
        {
            _exit(EXIT_SUCCESS);
        }
        int in = 0;
        int out = 0;
        while(in<read_result)
        {
            if (in<=read_result - old_len)
            {
                int flag_found = 1;
                for (int i=0; i<old_len;i++)
                {
                    if (buffer[in+i]!=old_val[i])
                    {
                        flag_found = 0;
                        break;
                    }
                }
                if (flag_found)
                {
                    for (int i=0; i<new_len;i++)
                    {
                        replaced[out+i] = new_val[i];
                    }
                    in+=old_len;
                    out+=new_len;
                    continue;
                }
            }
            replaced[out] = buffer[in];
            in++;
            out++;
        }
        write_all(central_pipe[1], replaced, out);
    }
}

void find(char * pattern)
{
    int pat_len = strlen(pattern);
    char buffer[BUFFER_SIZE];
    while(1)
    {
        int read_result = checked_call(read_until(central_pipe[0], buffer, BUFFER_SIZE, '\n'), "read in find");
        if (read_result == 0)
        {
            _exit(EXIT_SUCCESS);
        }
        for (int i=0; i<=read_result-pat_len;i++)
        {
            int flag_found = 1;
            for (int j= 0; j<pat_len;j++)
            {
                if (buffer[i+j]!=pattern[j])
                {
                    flag_found = 0;
                    break;
                }
            }
            if (flag_found)
            {
                write_all(STDOUT_FILENO, buffer, read_result);
                break;
            }
        }
    }
}

int main(int argc, char ** argv)
{
    if (argc<4)
    {
        checked_call(-1, "too few args");
    }
    if (strlen(argv[2])<strlen(argv[1]))
    {
        checked_call(-1, "wrong args");
    }
    checked_call(pipe(central_pipe), "error when creating pipe");
    int f = fork();
    if (f)
    {
        checked_call(close(central_pipe[1]), "error when closing pipe");
        find(argv[3]);
    }
    else
    {
        replace(argv[1], argv[2]);
    }
    return EXIT_SUCCESS;
}
