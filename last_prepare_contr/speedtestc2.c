#include<errno.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>

#define BUFFER_SIZE 4096

int central_pipe[2];
char big_buf[BUFFER_SIZE];
int bytes_in_buffer = 0;

void after_found_word(char * buf, int n)
{
    memcpy(buf, big_buf, n);
    memmove(big_buf, big_buf+n, bytes_in_buffer - n);
    bytes_in_buffer -=n;
}

int read_until(int fd, char * buf, int count, char delimiter)
{
    int have_read = 0;
    while (have_read<count)
    {
        for (int i = 0; i<bytes_in_buffer && i+have_read<count; i++)
        {
            if (big_buf[i]==delimiter)
            {
                after_found_word(buf+have_read, i+1);
                return have_read+i+1;
            }
        }
        int bytes_to_copy = bytes_in_buffer+have_read>count? count - have_read: bytes_in_buffer;
        memcpy(buf+have_read, big_buf, bytes_to_copy);
        have_read+=bytes_to_copy;
        bytes_in_buffer=0;
        int read_result = read(fd, big_buf, BUFFER_SIZE);
        if (read_result<=0)
        {
            if (have_read == 0)
            {
                return read_result;
            }
            else
            {
                return have_read;
            }

        }        
        bytes_in_buffer = read_result;
    }
    return count;
}

int checked_call(int result, char * msg)
{
    if (result == -1)
    {
        perror(msg);
        _exit(EXIT_FAILURE);
    }
    return result;
}

void write_all(int fd, char * buffer, int count)
{
    int already_wrote  = 0;
    while (already_wrote<count)
    {
        already_wrote+=checked_call(write(fd, buffer+already_wrote, count-already_wrote), "error while writing");
    }
}

void replace(char * old_val, char * new_val)
{
    char buffer [BUFFER_SIZE];
    char replaced[BUFFER_SIZE];
    int old_len = strlen(old_val);
    int new_len = strlen(new_val);
    while(1)
    {
        int read_result = checked_call(read_until(STDIN_FILENO, buffer, BUFFER_SIZE, '\n'),"error while reading");
        if (read_result == 0)
        {
            _exit(EXIT_SUCCESS);
        }
        int in = 0;
        int out = 0;
        while(in<read_result)
        {
            if(in<=read_result-old_len)
            {
                int flag_found = 1;
                for (int j =0;j<old_len;j++)
                {
                    if (buffer[in+j]!=old_val[j])
                    {
                        flag_found = 0;
                        break;
                    }
                }
                if (flag_found)
                {
                    for (int j=0; j<new_len;j++)
                    {
                        replaced[out+j] = new_val[j];
                    }
                    in+=old_len;
                    out+=new_len;
                    continue;
                }
            }
            replaced[out] = buffer[in];
            in++;
            out++;
        }
        write_all(central_pipe[1], replaced, out);
    }
}

void find(char * pattern)
{
    int pat_len = strlen(pattern);
    char buffer[BUFFER_SIZE];
    while (1)
    {
        int read_result = checked_call(read_until(central_pipe[0], buffer, BUFFER_SIZE, '\n'),"error while reading");
        if (read_result == 0)
        {
            _exit(EXIT_SUCCESS);
        }
        for (int i=0;i<=read_result - pat_len;i++)
        {
            int flag_found = 1;
            for (int j =0; j<pat_len; j++)
            {
                if (buffer[i+j]!=pattern[j])
                {
                    flag_found = 0;
                    break;
                }
            }
            if (flag_found)
            {
                write_all(STDOUT_FILENO, buffer, read_result);
                break;
            }
        }
    }
}

int main(int argc, char ** argv)
{
    if (argc<4)
    {
        checked_call(-1, "too few args");
    }
    if (strlen(argv[2])<strlen(argv[1]))
    {
        checked_call(-1, "wrong args");
    }
    checked_call(pipe(central_pipe), "error when creating pipe");
    int f = fork();
    if (f)
    {
        checked_call(close(central_pipe[1]), "error when closing pipe");
        find(argv[3]);
    }
    else
    {
        replace(argv[1], argv[2]);
    }
    return EXIT_SUCCESS;
}
