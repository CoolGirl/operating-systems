#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>

#define BUFFER_SIZE 4096

char big_buf[BUFFER_SIZE]; //it's really bigger than the others
size_t bytes_in_buffer = 0;

void after_found_word(void *buf, size_t n)//it must take current_read%BUFFER_SIZE
{
    memcpy(buf, big_buf, n);
    memmove(big_buf, big_buf + n, bytes_in_buffer - n);
    bytes_in_buffer -= n;
}

ssize_t read_until(int fd, void *buf, size_t count, char delimiter) {
    size_t current_read = 0;
    while (current_read <
           count)  // contract:big_buf must be dumped into buf and big_buf must point to original beginning
        // of big_buf
    {
        for (size_t j = 0; j < bytes_in_buffer && current_read + j < count; j++) {
            if (*((char *) big_buf + j) == delimiter) {
                after_found_word(buf + current_read, j + 1);
                return current_read + j + 1;
            }
        }
        size_t bytes_to_copy = current_read + bytes_in_buffer <= count ? bytes_in_buffer : count - current_read;
        memcpy(buf + current_read, big_buf, bytes_to_copy);
        current_read += bytes_to_copy;
        bytes_in_buffer = 0;
        ssize_t result = read(fd, big_buf, BUFFER_SIZE);
        if (result <= 0) {
            if (current_read == 0) {
                return result;
            }
            else {
                return current_read;
            }
        }
        bytes_in_buffer = result;
    }
    return count;
}

int checked_call(int result, char * msg)
{
    if (result == -1)
    {
        perror(msg);
        _exit(EXIT_FAILURE);
    }
    return result;
}

void write_all(int fd, char * buffer, int count)
{
    int already_wrote = 0;
    while (already_wrote<count)
    {
        already_wrote += checked_call(write(fd, buffer+already_wrote, count - already_wrote), "write");
    }
}

int central_pipe[2];
void replace (char * old_val, char * new_val)
{
   char replaced[BUFFER_SIZE]; 
   char buffer[BUFFER_SIZE];
   int old_len = strlen(old_val);
   int new_len = strlen(new_val);
   while (1)
   {
       int read_result = checked_call(read_until(STDIN_FILENO, buffer, BUFFER_SIZE, '\n'), "error while reading");
       if (read_result == 0)
       {
           _exit(EXIT_SUCCESS);
       }
       int i = 0;
       int out = 0;
       while (i<read_result)
       {
          if (i<=read_result - old_len)
          {
              int flag_found = 1;
               for (int j =0; j<old_len;j++)
               {
                   if (buffer[i+j]!=old_val[j])
                   {
                       flag_found = 0;
                       break;
                   }
               }
               if (flag_found)
               {
                   for (int j =0; j<new_len;j++)
                   {
                       replaced[out+j]= new_val[j];
                   }
                   i+=old_len;
                   out+=new_len;
                   continue;
               }
          }
          replaced[out] = buffer[i];
          i++;
          out++;
        }
     write_all(central_pipe[1], replaced, out);
   }
}

void find(char * pattern)
{
    char buffer[BUFFER_SIZE];
    int pattern_len = strlen(pattern);
    while(1)
    {
        int read_result= checked_call(read_until(central_pipe[0], buffer, BUFFER_SIZE, '\n'), "read_until");
        if (read_result == 0)
        {
            _exit(EXIT_SUCCESS);
        }
        for (int i=0;i<=read_result-pattern_len;i++)
        {
            int flag_found = 1;
            for (int j = 0; j<pattern_len; j++)
            {
               if (buffer[i+j]!=pattern[j])
               {
                   flag_found = 0;
                   break;
               }
            }
            if (flag_found)
            {
                write_all(STDOUT_FILENO, buffer, read_result);
                break;
            }
        }
        
    }
}

int main(int argc, char ** argv)
{
    if (argc<4)
    {
       checked_call(-1, "too few args"); 
    }
    checked_call(pipe(central_pipe), "error when creating pipe");
    int f = fork();
    if (f)
    {
        checked_call(close(central_pipe[1]), "error when closing fd");
        find(argv[3]);
    }
    else
    {
        replace(argv[1], argv[2]);
    }
}

		

