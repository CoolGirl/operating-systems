#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define BUFFER_SIZE 4096

int checked_call(int result, char* error_msg) {
    if (result == -1) {
        perror(error_msg);
        _exit(EXIT_FAILURE);
    }
    return result;
}

char big_buf[BUFFER_SIZE]; //it's really bigger than the others
size_t bytes_in_buffer = 0;

void after_found_word(void *buf, size_t n)//it must take current_read%BUFFER_SIZE
{
    memcpy(buf, big_buf, n);
    memmove(big_buf, big_buf + n, bytes_in_buffer - n);
    bytes_in_buffer -= n;
}

#define FOUND_DELIMITER 1
#define FOUND_EOF 2

ssize_t read_until(int fd, void *buf, size_t count, char delimiter,
                   int *status) {
    size_t current_read = 0;
    while (current_read <
           count)  // contract:big_buf must be dumped into buf and big_buf must point to original beginning
        // of big_buf
    {
        for (size_t j = 0; j < bytes_in_buffer && current_read + j < count; j++) {
            if (*((char *) big_buf + j) == delimiter) {
                after_found_word(buf + current_read, j + 1);
                ((char*)buf)[j] = 0;
                *status = FOUND_DELIMITER;
                return current_read + j + 1;
            }
        }
        size_t bytes_to_copy = current_read + bytes_in_buffer <= count ? bytes_in_buffer : count - current_read;
        memcpy(buf + current_read, big_buf, bytes_to_copy);
        current_read += bytes_to_copy;
        bytes_in_buffer = 0;
        ssize_t result = read(fd, big_buf, BUFFER_SIZE);
        if (result <= 0) {
            *status = FOUND_EOF;
            ((char*)buf)[current_read] = 0;
            if (current_read == 0) {
                return result;
            }
            else {
                return current_read;
            }
        }
        bytes_in_buffer = result;
    }
    return count;
}

void split(char* str, int n, char delimiter, char** result) {
    char* last_start = str;
    for (int i = 0; i < n; ++i) {
        if (str[i] == delimiter || i == n-1) {
           if (str[i] == delimiter) {
               str[i] = '\0';
           }
           *(result++) = last_start;
           last_start = str + i + 1;
        }
    }
    *result = NULL;
}

void exec_with_fds(char** args, int fd_in, int fd_out) {
    checked_call(dup2(fd_in, STDIN_FILENO), "dup2");
    checked_call(dup2(fd_out, STDOUT_FILENO), "dup2");
    checked_call(execvp(args[0], args), "exec");    
}

void fork_exec(char** args, int fd_in, int fd_out) {
    int fork_result = checked_call(fork(), "fork");
    if (!fork_result) {
        exec_with_fds(args, fd_in, fd_out);
    }
}

#define MAX_ARGS 256

char line_buffer[BUFFER_SIZE];

int main(int argc, char* argv[]) {
    int right_pipe[2];

    int left_pipe = open("/dev/null", O_RDONLY); 

    checked_call(pipe(right_pipe), "pipe");

    while (1) {
        int status;

        int line_len = read_until(STDIN_FILENO, line_buffer,
                                  BUFFER_SIZE, '\n', &status);
        checked_call(line_len, "read_until");
        if (line_len == 0)
        {
            _exit(EXIT_FAILURE);
        }
        char* splitted[MAX_ARGS];
        split(line_buffer, line_len, ' ', splitted);

        int fd_out = right_pipe[1];

        if (status == FOUND_EOF) {
            exec_with_fds(splitted, left_pipe, STDOUT_FILENO);
        } else if (status == FOUND_DELIMITER) {
            fork_exec(splitted, left_pipe, fd_out);
        }

        checked_call(close(right_pipe[1]), "close");
        checked_call(close(left_pipe), "close");

        left_pipe = right_pipe[0];
        checked_call(pipe(right_pipe), "pipe");
    }
}
