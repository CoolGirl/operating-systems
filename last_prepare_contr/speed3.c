
#include<errno.h>
#include<string.h>
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>

#define BUFFER_SIZE 4096
#define FOUND_EOF 1
#define FOUND_DELIMITER 2

int checked_call(int result, char * msg)
{
    if (result == -1)
    {
        perror(msg);
        _exit(EXIT_FAILURE);
    }
    return result;
}

char big_buf[BUFFER_SIZE];
int bytes_in_buffer = 0;

void after_found_word(char * buf, int n)
{
    memcpy(buf, big_buf, n);
    memmove(big_buf, big_buf+n, bytes_in_buffer - n);
    bytes_in_buffer -= n;
}

int read_until(int fd, char * buf, int count, char delimiter, int* status)
{
    int current_read = 0;
    while(current_read<count)
    {
        for (int i =0; i<bytes_in_buffer && i+current_read<count; i++)
        {
            if (big_buf[i]==delimiter)
            {
                *status = FOUND_DELIMITER;
                after_found_word(buf+current_read, i+1);
                buf[i]=0;
                return current_read +i+1;
            }
        }
        int bytes_to_copy = bytes_in_buffer+current_read<count? bytes_in_buffer: count- current_read;
        memcpy(buf+current_read, big_buf, bytes_to_copy);
        current_read+=bytes_to_copy;
        bytes_in_buffer = 0;
        int read_result =  read(fd, big_buf, BUFFER_SIZE);
        if (read_result<=0)
        {
           buf[current_read]=0; 
            if (read_result == 0)
            {
                *status = FOUND_EOF;
            }
            if (current_read == 0)
            {
                return read_result;
            }
            else
            {
                return current_read;
            }
        }
        bytes_in_buffer = read_result;
    }
    return count;
}

void split(char * buf, int count, char splitter,  char ** splitted)
{
    char * last_start = buf;
    for (int i=0;i<count;i++)
    {
        if (buf[i]==splitter || i==count-1)
        {
            if (buf[i]==splitter)
            {
                buf[i]='\0';
            }
        *(splitted++) = last_start;
        last_start = buf+i+1;
        }
    }
    *splitted = NULL;
   

}
void exec_with_fds(int fd_in, int fd_out, char ** args)
{
    checked_call(dup2(fd_in,STDIN_FILENO), "dup2");
    checked_call(dup2(fd_out,STDOUT_FILENO), "dup2"); 
    checked_call(execvp(args[0],args), "exec"); 
}

void fork_exec_with_fds(int fd_in, int fd_out, char ** args)
{
    int f = checked_call(fork(), "fork");
    if (!f)
    {
        exec_with_fds(fd_in,fd_out, args);
    }
}
#define MAX_ARGS 256
int main(int argc, char ** argv)
{
    char line_buffer[BUFFER_SIZE];
    int right_pipe[2];
    int left_pipe = open("/dev/null", O_RDONLY);
    checked_call(pipe(right_pipe), "pipe in main");
    while(1)
    {
        int status;
        int line_len = checked_call(read_until(STDIN_FILENO, line_buffer, BUFFER_SIZE, '\n', &status), "read");
        char* splitted[MAX_ARGS];
        split(line_buffer, line_len, ' ', splitted); 
        int fd_out = right_pipe[1];
        if (status==FOUND_EOF)
        {
            exec_with_fds(left_pipe, STDOUT_FILENO, splitted);
        }
        else if (status==FOUND_DELIMITER)
        {
            fork_exec_with_fds(left_pipe, fd_out, splitted);
        }
        checked_call(close(left_pipe), "close");
        checked_call(close(right_pipe[1]), "close");

        left_pipe = right_pipe[0];
        checked_call(pipe(right_pipe), "pipe");
    }
    return EXIT_SUCCESS;
}
