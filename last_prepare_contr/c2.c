#include <stdlib.h>
#include<stdio.h>
#include<errno.h>
#include<unistd.h>
#include<string.h>

#define BUFFER_SIZE 10

int central_pipe[2];

int checked_call(int result, char * msg)
{
    if (result == -1)
    {
        perror(msg);
        _exit(EXIT_FAILURE);
    }
    return result;
}

void write_all(int fd, char * buffer, int count)
{
    int already_wrote = 0;
    while (already_wrote<count)
    {
        int write_result = checked_call(write(fd, buffer+already_wrote, count-already_wrote), "error while writing");
        already_wrote+=write_result;
    }
}

void replace(char * map1, char * map2)
{
    int map_len = strlen(map1);
    char buffer[BUFFER_SIZE];
    while(1)
    {
        int read_result = checked_call(read(STDIN_FILENO, buffer, BUFFER_SIZE), "error while reading");
        if (read_result == 0)
        {
            close(central_pipe[1]);
            return;
        }
        for (int i=0; i<read_result; i++)
        {
            for (int j=0; j<map_len; j++)
            {
                if (buffer[i]==map1[j])
                {
                    buffer[i] = map2[j];
                    break;
                }
            }
        }
        write_all(central_pipe[1], buffer, read_result); 
    }
}

void find(char * pattern)
{
    int pattern_len = strlen(pattern);
    char buffer[BUFFER_SIZE];
    int current_pos = 0;
    int bytes_in_buffer = 0;
    int i = 0;
    int flag_pattern = 0;
    while(1)
    {
        int read_result = checked_call(read(central_pipe[0], buffer+bytes_in_buffer, BUFFER_SIZE - bytes_in_buffer), "error while reading");
        if (read_result == 0)
        {
            if (flag_pattern)
            {
            write_all(STDOUT_FILENO, buffer+current_pos, bytes_in_buffer-current_pos);
            }
            close(central_pipe[0]);
            _exit(EXIT_SUCCESS); 
        }
        bytes_in_buffer+=read_result;
        int will_be_in_buffer = bytes_in_buffer;
        i = 0; 
        while(i < bytes_in_buffer)
        {
            if (buffer[i] == '\n')
            {
                if (flag_pattern)
                {
                    write_all(STDOUT_FILENO, buffer+current_pos, i-current_pos+1);
                }
                will_be_in_buffer -= (i-current_pos+1);
                current_pos = i+1; 
                flag_pattern = 0;
            }
            if (!flag_pattern && pattern_len<=bytes_in_buffer && i<=bytes_in_buffer-pattern_len)
            {
                flag_pattern = 1;
                for (int j = 0; j<pattern_len; j++)
                {
                    if (buffer[i+j]!=pattern[j])
                    {
                        flag_pattern = 0;
                        break;
                    }
                }
            }
            i++;//!
        }
        if (bytes_in_buffer!=will_be_in_buffer)
        {

            memmove(buffer, buffer+bytes_in_buffer-will_be_in_buffer, will_be_in_buffer);
            current_pos = 0; //will_be_in_buffer-pattern_len;
            i = current_pos;
            bytes_in_buffer=will_be_in_buffer;
        }
    }
}

    int main(int argc, char ** argv)
    {
        if (argc<4)
        {
            checked_call(-1, "too few args");
        }
        if (strlen(argv[2])<strlen(argv[1]))
        {
            checked_call(-1, "wrong args");
        }
        checked_call(pipe(central_pipe),"error while creating pipe");
        int f = fork();
        if (f)
        {
            checked_call(close(central_pipe[1]), "error when closing pipe");
            find(argv[3]);  
        }
        else
        {
            replace(argv[1], argv[2]);
        }
        return EXIT_SUCCESS;
    }
